# Análise de Perfil de Crédito

Projeto desafio para análise de perfil de crédito

## API de Perfil

API Rest para calcular e consultar score dos clientes

## API de Score

API Rest que simula o calculo do score. 
* Um loading de até 8 segundos foi adicionado para simular a execução de um serviço lento.
* **Foi adicionado uma probalididade de 50% de chance de dar erro (apenas para simular)**

## Pré-requisitos

* docker e docker-compose instalados

### Como executar o projeto?

Basta executar o comando abaixo na raiz do repositório. Os três serviços serão executados.
* Api de Perfil
* Api de Score
* RabbitMQ para processamento de mensagens

```
docker-compose up --build
```

### Visualizando as execuçoes

* http://localhost:8082/ - Frontend para visualizar as requições
* http://localhost:8082/swagger-ui/