package com.perfil;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiPerfilApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiPerfilApplication.class, args);
	}

}
