package com.perfil.config;

import lombok.RequiredArgsConstructor;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@RequiredArgsConstructor
public class RabbitProcuderConfig {

    public static final String EXCHANGE = "simpleMessageExchange";

    public static final String SCORE_QUEUE = "score.queue";
    public static final String SCORE_QUEUE_KEY_BIDING = "scoreQueue";

    public static final String SCORE_REPLYABLE_QUEUE = "scoreReplyable.queue";
    public static final String SCORE_REPLYABLE_QUEUE_KEY_BIDING = "scoreReplyableQueue";

    public static final String SCORE_REPLY_QUEUE = "replyScore.queue";

    public static final String DEAD_LETTER_EXCHANGE = "deadLetterExchange";
    public static final String DEAD_LETTER_QUEUE = "deadLetter.queue";
    public static final String DEAD_LETTER_QUEUE_KEY_BIDING = "deadLetterQueue";

    @Bean
    public DirectExchange scrapperExchange() {
        return new DirectExchange(EXCHANGE);
    }

    @Bean
    public Queue scoreQueue() {
        return QueueBuilder.durable(SCORE_QUEUE)
                .withArgument("x-max-priority", 5)
                .withArgument("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE)
                .withArgument("x-dead-letter-routing-key", DEAD_LETTER_QUEUE_KEY_BIDING).build();
    }

    @Bean
    public Binding scoreQueueBinding() {
        return BindingBuilder.bind(scoreQueue()).to(scrapperExchange()).with(SCORE_QUEUE_KEY_BIDING);
    }

    @Bean
    public Queue scoreReplyableQueue() {
        return QueueBuilder.durable(SCORE_REPLYABLE_QUEUE)
                .withArgument("x-max-priority", 5)
                .withArgument("x-dead-letter-exchange", DEAD_LETTER_EXCHANGE)
                .withArgument("x-dead-letter-routing-key", DEAD_LETTER_QUEUE_KEY_BIDING).build();
    }

    @Bean
    public Binding scoreReplyableQueueBinding() {
        return BindingBuilder.bind(scoreReplyableQueue()).to(scrapperExchange()).with(SCORE_REPLYABLE_QUEUE_KEY_BIDING);
    }

    //dead letter
    @Bean
    public DirectExchange deadLetterExchange() {
        return new DirectExchange(DEAD_LETTER_EXCHANGE);
    }

    @Bean
    public Queue deadLetterQueue() {
        return QueueBuilder.durable(DEAD_LETTER_QUEUE).build();
    }

    @Bean
    public Binding deadLetterQueueBinding() {
        return BindingBuilder.bind(deadLetterQueue()).to(deadLetterExchange()).with(DEAD_LETTER_QUEUE_KEY_BIDING);
    }

    @Bean
    public Jackson2JsonMessageConverter jackson2JsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AsyncRabbitTemplate asyncTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jackson2JsonMessageConverter());

        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer(connectionFactory);
        container.setMaxConcurrentConsumers(1);
        container.setQueueNames(SCORE_REPLY_QUEUE);
        return new AsyncRabbitTemplate(rabbitTemplate, container);
    }

    @Bean
    public Queue replyQueue() {
        return new Queue(SCORE_REPLY_QUEUE);
    }
}
