package com.perfil.controller;

import com.perfil.domain.PerfilCredito;
import com.perfil.domain.Pessoa;
import com.perfil.service.ScoreService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Api(value = "PerfilController")
public class PerfilController {

    private final ScoreService scoreService;

    @PostMapping("/perfil/score")
    public PerfilCredito calcularScore(@RequestBody Pessoa pessoa) {
        return scoreService.calcularScore(pessoa);
    }


}
