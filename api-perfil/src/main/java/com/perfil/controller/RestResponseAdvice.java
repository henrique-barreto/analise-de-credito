package com.perfil.controller;

import com.perfil.exceptions.ScoreEmProcessamentotException;
import com.perfil.exceptions.ScoreServiceTimeoutException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class RestResponseAdvice {

    @ExceptionHandler(value = {ScoreServiceTimeoutException.class})
    public ResponseEntity<Map<String, String>> handleBadRequestException(RuntimeException ex) {
        Map<String, String> map = new HashMap<>();
        map.put("exception", ScoreServiceTimeoutException.class.getName());
        map.put("message", ex.getMessage());
        return new ResponseEntity<>(map, null, HttpStatus.ACCEPTED);
    }

    @ExceptionHandler(value = {ScoreEmProcessamentotException.class})
    public ResponseEntity<Map<String, String>> handleScoreEmProcessamentotException(RuntimeException ex) {
        Map<String, String> map = new HashMap<>();
        map.put("exception", ScoreEmProcessamentotException.class.getName());
        map.put("message", ex.getMessage());
        return new ResponseEntity<>(map, null, HttpStatus.ACCEPTED);
    }
}
