package com.perfil.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class PerfilCredito {

    private String nome;

    private String cpf;

    private int score;

    private BigDecimal valorCredito;

    private LocalDateTime dataCalculo;

}
