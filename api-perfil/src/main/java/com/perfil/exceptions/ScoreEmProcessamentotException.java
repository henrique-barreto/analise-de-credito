package com.perfil.exceptions;

public class ScoreEmProcessamentotException extends RuntimeException {

    public ScoreEmProcessamentotException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScoreEmProcessamentotException(String message) {
        super(message);
    }
}
