package com.perfil.exceptions;

public class ScoreServiceTimeoutException extends RuntimeException {

    public ScoreServiceTimeoutException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScoreServiceTimeoutException(String message) {
        super(message);
    }
}
