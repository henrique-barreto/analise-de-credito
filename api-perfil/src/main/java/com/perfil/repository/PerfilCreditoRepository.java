package com.perfil.repository;

import com.perfil.domain.PerfilCredito;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Simulando um repositorio de perfis de credito
 */
@Repository
public class PerfilCreditoRepository {

    private final Map<String, PerfilCredito> perfisMap = new ConcurrentHashMap<>();

    private static final long TEMPO_CACHE_EM_SEGUNDOS = 15;

    public PerfilCredito salvar(PerfilCredito perfilCredito) {
        perfilCredito.setDataCalculo(LocalDateTime.now());
        perfisMap.put(perfilCredito.getCpf(), perfilCredito);
        return perfilCredito;
    }

    public Optional<PerfilCredito> buscar(String cpf) {
        PerfilCredito perfilCredito = perfisMap.get(cpf);
        if (Objects.isNull(perfilCredito)) {
            return Optional.empty();
        }

        if (perfilCredito.getDataCalculo().isBefore(LocalDateTime.now().minusSeconds(TEMPO_CACHE_EM_SEGUNDOS))) {
            perfisMap.remove(cpf);
            return Optional.empty();
        }

        return Optional.of(perfilCredito);
    }


}
