package com.perfil.service;

import com.perfil.config.RabbitProcuderConfig;
import com.perfil.domain.PerfilCredito;
import com.perfil.domain.Pessoa;
import com.perfil.exceptions.ScoreEmProcessamentotException;
import com.perfil.exceptions.ScoreServiceTimeoutException;
import com.perfil.repository.PerfilCreditoRepository;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.AsyncRabbitTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.*;

@Service
@Slf4j
@RequiredArgsConstructor
public class ScoreService {

    private final AsyncRabbitTemplate asyncRabbitTemplate;

    private final PerfilCreditoRepository perfilCreditoRepository;

    private final Map<Future<PerfilCredito>, Pessoa> futures = new ConcurrentHashMap<>();

    @PostConstruct
    private void postConstruct() {
        new Thread(this::verificaPorFuturesFinalizados).start();
    }

    @SneakyThrows
    public PerfilCredito calcularScore(Pessoa pessoa) {
        Optional<PerfilCredito> perfilCredito = perfilCreditoRepository.buscar(pessoa.getCpf());
        if (perfilCredito.isPresent()) {
            return perfilCredito.get();
        }

        if (possuiScoreEmProcessamento(pessoa.getCpf())) {
            throw new ScoreEmProcessamentotException("Score ainda está em processamento...");
        }

        try {
            Future<PerfilCredito> future = enviarEventoParaCalculoDoScore(pessoa);
            futures.put(future, pessoa);
            PerfilCredito perfil = future.get(2, TimeUnit.SECONDS);
            perfilCreditoRepository.salvar(perfil);
            log.info("Perfil de credito - score: {}, valor: {}", perfil.getScore(), perfil.getValorCredito());
            return perfil;
        } catch (TimeoutException e) {
            throw new ScoreServiceTimeoutException("O calculo do score está em processamento... tente novamente mais tarde ...", e);
        } catch (InterruptedException | ExecutionException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private Future<PerfilCredito> enviarEventoParaCalculoDoScore(Pessoa pessoa) {
        log.info("Enviamento evento para calculo de score para pessoa: {}", pessoa.getNome());
        return asyncRabbitTemplate.convertSendAndReceiveAsType(RabbitProcuderConfig.EXCHANGE,
                RabbitProcuderConfig.SCORE_REPLYABLE_QUEUE_KEY_BIDING, pessoa, new ParameterizedTypeReference<>() {
                });
    }

    private boolean possuiScoreEmProcessamento(String cpf) {
        return futures.entrySet().stream()
                .anyMatch(entry -> entry.getValue().getCpf().equals(cpf));
    }

    @SneakyThrows
    private void verificaPorFuturesFinalizados() {
        while (true) {
            TimeUnit.SECONDS.sleep(1);
            for (Iterator<Map.Entry<Future<PerfilCredito>, Pessoa>> it = futures.entrySet().iterator(); it.hasNext(); ) {
                Map.Entry<Future<PerfilCredito>, Pessoa> entry = it.next();
                Future<PerfilCredito> future = entry.getKey();
                if (future.isDone() || future.isCancelled()) {
                    it.remove();
                    try {
                        PerfilCredito perfilCredito = future.get(2, TimeUnit.SECONDS);
                        perfilCreditoRepository.salvar(perfilCredito);
                    } catch (Exception e) {
                        log.error("Task cancelada.. nao faz nada");
                    }
                }
            }

        }
    }
}
