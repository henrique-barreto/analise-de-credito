package com.score.apiscore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiScoreApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiScoreApplication.class, args);
    }

}
