package com.score.apiscore.controller;

import com.score.apiscore.exceptions.BadRequestException;
import com.score.apiscore.exceptions.InternalServerErrorException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.HashMap;
import java.util.Map;

@RestControllerAdvice
public class RestResponseAdvice {

    @ExceptionHandler(value = {BadRequestException.class})
    public ResponseEntity<Map<String, String>> handleBadRequestException(RuntimeException ex) {
        Map<String, String> map = new HashMap<>();
        map.put("message", ex.getMessage());
        return new ResponseEntity<>(map, null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {InternalServerErrorException.class})
    public ResponseEntity<Map<String, String>> handleInternalServerErrorException(RuntimeException ex) {
        Map<String, String> map = new HashMap<>();
        map.put("message", ex.getMessage());
        return new ResponseEntity<>(map, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
