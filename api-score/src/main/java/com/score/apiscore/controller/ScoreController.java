package com.score.apiscore.controller;

import com.score.apiscore.domain.PerfilCredito;
import com.score.apiscore.domain.Pessoa;
import com.score.apiscore.service.ScoreService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController("/score")
public class ScoreController {

    private final ScoreService scoreService;

    public ScoreController(ScoreService scoreService) {
        this.scoreService = scoreService;
    }

    @PostMapping("/calcular")
    public PerfilCredito calcularScoreCredito(@RequestBody Pessoa pessoa) {
        return scoreService.calcularScore(pessoa);
    }

}
