package com.score.apiscore.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
public class PerfilCredito {

    private String nome;

    private String cpf;

    private int score;

    private BigDecimal valorCredito;

    public PerfilCredito(int score, BigDecimal valorCredito, String cpf, String nome) {
        this.score = score;
        this.valorCredito = valorCredito;
        this.cpf = cpf;
        this.nome = nome;
    }

}
