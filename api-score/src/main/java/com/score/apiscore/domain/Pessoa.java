package com.score.apiscore.domain;

import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class Pessoa {

    private String nome;

    private int idade;

    private String cpf;

    private int dependentes;

    private BigDecimal renda;

}
