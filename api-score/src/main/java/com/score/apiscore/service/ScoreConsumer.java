package com.score.apiscore.service;

import com.score.apiscore.domain.PerfilCredito;
import com.score.apiscore.domain.Pessoa;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class ScoreConsumer {

    private final ScoreService scoreService;

    @RabbitListener(queues = "score.queue", containerFactory = "scoreContainer")
    public void consumeScoreQueue(@Payload Pessoa pessoa) throws Exception {
        log.info("consumindo evento para calculo do score: {}", pessoa.getNome());
        scoreService.calcularScore(pessoa);
    }

    @RabbitListener(queues = "scoreReplyable.queue", containerFactory = "scoreContainer")
    public PerfilCredito consumeScoreReplyableQueue(@Payload Pessoa pessoa) {
        log.info("consumindo evento para calculo do score: {}", pessoa.getNome());
        PerfilCredito perfilCredito = scoreService.calcularScore(pessoa);
        log.info("Pessoa: {} com score {} e resuiltado {}", perfilCredito.getNome(), perfilCredito.getScore(), perfilCredito.getValorCredito());
        return perfilCredito;
    }
}
