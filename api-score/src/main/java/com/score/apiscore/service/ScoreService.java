package com.score.apiscore.service;

import com.score.apiscore.domain.PerfilCredito;
import com.score.apiscore.domain.Pessoa;

import static org.apache.commons.lang3.StringUtils.*;

import com.score.apiscore.exceptions.BadRequestException;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ScoreService {

    private final Map<String, PerfilCredito> mapPessoaCredito = new HashMap<>();

    public ScoreService() {
        mapPessoaCredito.put("Pedro", new PerfilCredito(350, BigDecimal.ZERO, "11111111111", "Pedro"));
        mapPessoaCredito.put("Joao", new PerfilCredito(600, BigDecimal.valueOf(500), "22222222222", "Joao"));
        mapPessoaCredito.put("Jose", new PerfilCredito(450, BigDecimal.valueOf(500), "33333333333", "Jose"));
        mapPessoaCredito.put("Mateus", new PerfilCredito(880, BigDecimal.valueOf(5000), "44444444444", "Mateus"));
        mapPessoaCredito.put("Maria", new PerfilCredito(800, BigDecimal.valueOf(2500), "55555555555", "Maria"));
        mapPessoaCredito.put("Fernanda", new PerfilCredito(300, BigDecimal.ZERO, "66666666666", "Fernanda"));
    }

    @SneakyThrows
    public PerfilCredito calcularScore(Pessoa pessoa) {
        //delay forçado de ate 4 segundos...
        TimeUnit.SECONDS.sleep(new Random().nextInt(4));

        //50%  de chance de dar erro
        if (new Random().nextInt(10) > 4) {
            log.error("Simulando erro ao processar calculo do score");
            throw new RuntimeException("Erro ao processar calculo do score para: " + pessoa.getNome());
        }

        //simulando calculo do score
        PerfilCredito perfilCredito = mapPessoaCredito.get(capitalize(stripAccents(pessoa.getNome())));
        if (Objects.isNull(perfilCredito)) {
            throw new BadRequestException("Pessoa com nome " + pessoa.getNome() + " não encontrada.");
        }
        return perfilCredito;
    }
}
